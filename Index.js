const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const app = express();
const warning = 'password';
const port = 8000;

app.get('/:name', (req, res) => {
    res.send(req.params.name);
});

app.get("/encode/:name", (req, res) => {
    res.send(Vigenere.Cipher(warning).crypt(req.params.name));
});
app.get("/decode/:name", (req, res) => {
    res.send(Vigenere.Decipher(warning).crypt(req.params.name));
});

app.listen(port, () => {
    console.log('We are live on ' + port);
});